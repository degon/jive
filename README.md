[![Documentation Status](https://readthedocs.org/projects/jive/badge/?version=latest)](http://jive.readthedocs.io/en/latest/?badge=latest)

[![Download](https://api.bintray.com/packages/tango-controls/maven/Jive/images/download.svg) ](https://bintray.com/tango-controls/maven/Jive/_latestVersion)

# jive
Jive is a standalone JAVA application designed to browse and edit the static TANGO database.

# Change Log

* V 7.27
    * Fix: Collection not displayed when change TangoHost.

* V 7.26
    * Fix: Wrong escape sequence when resource value contains '\\'
    * Fix: Wrong parsing when resource value contains an item equal to a special char [',' , '/' , '\\' , ':' , '-\>' ]
    